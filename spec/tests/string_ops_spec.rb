require 'spec_helper'

describe StringOps do
  context 'count_compress' do
    it 'compresses string by counting continuos characters' do
      expect(StringOps.count_compress('aaabbaacccccd')).to eql('3a2b2a5c1d')
    end

    it 'compresses 1-char string by counting continuos characters' do
      expect(StringOps.count_compress('a')).to eql('1a')
    end

    it 'returns the string as it is if the string is nil' do
      expect(StringOps.count_compress(nil)).to be_nil
    end

    it 'returns the string as it is if the string is empty' do
      expect(StringOps.count_compress('')).to eql('')
    end
  end
end
