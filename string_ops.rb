require 'active_support/all'

class StringOps
  class << self
    
    def count_compress(str)
      return str if str.blank?

      result = ''      
      current_char  = str[0]
      current_count = 1

      0.upto(str.size - 1) do |idx|
        current_count += 1 and next if current_char == str[idx+1]
        result = result + current_count.to_s + current_char
        current_char  = str[idx+1]
        current_count = 1
      end
      
      result
    end

  end  
end